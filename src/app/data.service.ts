import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  getUsers(){
    return this.http.get("https://jsonplaceholder.typicode.com/users"); // Récupère tout les utilisateurs
  }
  getUser(userID){
    return this.http.get("https://jsonplaceholder.typicode.com/users/"+ userID); // Selectionne un utilisateur via son ID
  }
  getPosts(){
    return this.http.get("https://jsonplaceholder.typicode.com/posts");
  }
}
